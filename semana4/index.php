<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Actividad 4 | transferencia de datos </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
            h1 {text-align: center; font-family: times; }
            
            .tg  {border-collapse:collapse;border-spacing:0;border-color:#999;}
            .tg td{font-family:Arial, sans-serif;font-size:14px;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}
            .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:bold;padding:5px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}
   
        </style>
</head>
    
<body>
<div class="container">
<h1>Envio de datos en formularios</h1>
<?php

        /*
        luis fernando loaiza acevedo
        DESARROLLO WEB CON PHP   */

require("escenario.php");
require("accion.php");

if(isset($_REQUEST["Enviar"])){
    
                $fila = $_POST['fila'];
                $puesto= $_POST['puesto'];
                $accion= $_POST['accion'];
                $StringEscenario=$_POST['ListEscenario'];
                
                $count=0;
                for($i=0;$i<5;$i++){
                    for($j=0;$j<5;$j++){
                        $count=5*$i+$j;                        
                        $ListEscenario[$i][$j]=substr($StringEscenario,$count,1);
                    }
                    $count=0;
                }
        
        $ListEscenario=Accion($fila,$puesto,$accion,$ListEscenario);        
        Escenario($ListEscenario);
}
//Se valida cuando el usario borra la informacion del formulario
else if(isset($_REQUEST["Reset"]) || !isset($_REQUEST["Enviar"])){
    $ListEscenario=array(array("L","L","L","L","L"),array("L","L","L","L","L"),array("L","L","L","L","L"),array("L","L","L","L","L"),array("L","L","L","L","L"));
    Escenario($ListEscenario);
}
?>

    <div class="container">
        <form class="row justify-content-md-center" method="post">
            <div class="form-group">
            <!-- Se separa el array $ListEscenario en un String y de oculta-->
            <input type="text" name="ListEscenario" value="<?php foreach ($ListEscenario as $fila) {foreach ($fila as $puesto){echo $puesto;}}?>" style="display:none" />
            
            <div class="form-group">
            <label for="exampleFormControlSelect1">Seleccione el numero de fila de su preferencia</label>
            <select class="form-control" name="fila">
                    <option size="4">1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
            </select>
            </div>
            <div class="form-group">
            <label for="exampleFormControlSelect1">Seleccione el numero de puesto de su preferencia</label>
                <select class="form-control" name="puesto">
                    <option size="4">1</option>
                    <option size="4">2</option>
                    <option size="4">3</option>
                    <option size="4">4</option>
                    <option size="4">5</option>
                </select>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="accion" value="R">
                <label class="form-check-label" for="inlineCheckbox1">Reservar</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="accion" value="V">
                <label class="form-check-label" for="inlineCheckbox1">Comprar</label>
            </div>

            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" name="accion" value="L">
                <label class="form-check-label" for="inlineCheckbox1">Liberar</label>
            </div>

            <input class="btn btn-outline-success" type="submit" value="Enviar" name="Enviar" />
            <input class="btn btn-outline-danger" type="submit" value="Borrar" name="Reset" />
                        
        </form>
    </div>



</div>

<!-- Footer -->
<footer class="page-footer font-small dark">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Luis Fernando Loaiza Acevedo: SENA
    <a href="#"> DESARROLLO WEB CON PHP</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->


    
    <!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

<!-- Option 2: jQuery, Popper.js, and Bootstrap JS
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
-->
</body>
</html>