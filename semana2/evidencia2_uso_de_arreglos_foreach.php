<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Uso de Arreglos!</title>

    <STYLE type="text/css">
       h1 {text-align: center; font-family: times; }
       h3 { text-align: center; font-family: didot;}
       .alert{ text-align: center;}
       table{ text-align: center; }
       
 </STYLE>


  </head>
  <body>
    <h1>Uso de arreglos en php</h1>
    <h3>Foreach</h3>
    <div class="alert alert-success" role="alert">
    recorriendo arreglos
    </div>

        <?php
         /* luis fernando loaiza acevedo  
          Desarrollo web con php 
            uso de arreglos*/

             $Colores = array (
                "Amarillo" => "armonía, sabiduría, agilidad, brillante.",
                "Azul" => "estabilidad, confianza, masculino, racionalidad.",
                "Rojo" => "pasión, violencia, fuego, seducción, poder, activo.",
                "Verde" => "naturaleza, crecimiento, fertilidad, dinero, aire libre.",
                "Violeta" => "poderoso, ambicioso, misterioso, dignidad, rico.",
                "Rosado" => "femenino, romance, inocencia, juvenil.",
                "Naranja" => "felicidad, entusiasmo, creatividad, éxito.",
             );

            $Directorio = array (
                array("Nombre","Dirección","Teléfono","Fecha de Cumpleaños","Color Favorito","Significado"),
                array("Juan Perez","Cra 45 #45-56","3456789","23/12/1997","Amarillo",""),
                array("Nancy Peña","Av. 34 #16-12","2135423","07/06/2000","Rojo",""),
                array("Pablo Manrrique","Cra 23 #12-19 Sur","3214567","12/10/1980","Verde",""),
                array("Pedro Alvarez","Cra 32 #12-57","3128596","12/10/1980","Negro",""),
                array("Oscar Jimenez","Cl 4 No. 13 - 90","8311128","01/08/1998","Violeta",""),
                array("Diana Velez","Cr 34 No. 89 - 90","8235841","02/08/1995","Rosado",""),
                array("Diego Soto","Av 34 No. 78 - 90","8358479","02/07/1994","Cafe",""),
           );

            for($i=1;$i<8;$i++){
                        $color=$Directorio[$i][4];
              if(empty($Colores[$color])){
                  $Directorio[$i][5]='No se encuentra el significado';
              }
              else {
                  $Directorio[$i][5]=$Colores[$color];
                }
            }
        ?>
     
    <table class="table">
            <?php
            //se crea tabla y se recorre array para mostrar informacion
                foreach ($Directorio as $fila){
                    echo "<tr>";
                        foreach ($fila as $celda){
                            echo "<td> $celda </td>";
                        }
                    echo "</tr>";
                }
            ?>
        </table>
<!-- Footer -->
<footer class="page-footer font-small dark">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Luis Fernando Loaiza Acevedo: SENA
    <a href="#"> DESARROLLO WEB CON PHP</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</html>