<?php

     class operaciones
     {  //definicion de funciones con los operadores basicos
         public static function sumar($num1, $num2){
             $suma = $num1 + $num2;
             return $suma;
         }
         public static function restar($num1, $num2){
            $resta = $num1 - $num2;
            return $resta;
        }
        public static function multiplicar($num1, $num2){
            $multiplicacion = $num1 * $num2;
            return $multiplicacion;
        }
        public static function dividir($num1, $num2){
            @$division = $num1 / $num2;
            if($num2 == 0) { // alertar en caso de dividir por 0
                echo '<div class="alert alert-danger" role="alert">
                ! Recuerda: la división por 0, no esta definida en la aplicacion :( !
              </div>';
            }else{
                return $division;
            }
        }
         
     }
     

?>