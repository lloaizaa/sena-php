<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <STYLE type="text/css">
            h1 {text-align: center; font-family: times; }
            h3 { text-align: center; font-family: didot;}
            .resultado{margin-top:20px;}
            
       
        </STYLE>

    <title>Actividad 3 - funciones!</title>
</head>

<body>
    
    <div class="container">
    <h1>Desarrollo web con PHP!</h1>
    <h3>uso de funciones</h3>
    <form id="formulario1" method="post" action="evidencia3.php">
        <div class="form-group">
            <input type="text" class="form-control" name="numero1" placeholder="ingrese el primer numero">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="numero2" placeholder="ingrese el segundo numero">
        </div> 
        <div class="form-group">
            <select class="form-control" name="opciones" >
                <option value="5"> seleccione operacion</option>
                <option value="0">Suma</option>
                <option value="1">Resta</option>
                <option value="2">Multiplicación</option>
                <option value="3">División</option>
            </select>
        </div>
        <button type="submit" name="calcular" class="btn btn-outline-success">Calcular</button>
        <button type="reset" class="btn btn-outline-danger">Borrar</button>
    </form>    
    </div>

    <div class="container resultado">
    <?php
    /* luis fernando loaiza acevedo  
          Desarrollo web con php 
            uso de funsiones*/

        include('biblioteca.php');
        if(empty($_POST['numero1']) and empty($_POST['numero2'])) {
            echo '<div class="alert alert-warning" role="alert">
            Por favor valide los datos ingresados ! algun campo esta vacio !
          </div>';
        }else{
                if(isset($_REQUEST['calcular'])){
                    $n1 = $_REQUEST['numero1'];
                    $n2 = $_REQUEST['numero2'];
                    $op = $_REQUEST['opciones'];
        //casos de resultados segun la operacion seleccionada
                    switch ($op){
                        case 0:
                            echo "<div class=\"alert alert-success\" role=\"alert\">la suma de los dos numeros ingresados es: ".operaciones::sumar($n1,$n2)."</div>";
                        break;
                        case 1:
                            echo "<div class=\"alert alert-success\" role=\"alert\">la resta de los dos numeros ingresados es: ".operaciones::restar($n1,$n2)."</div>";
                        break;
                        case 2:
                            echo "<div class=\"alert alert-success\" role=\"alert\">la multiplicación de los dos numeros ingresados es: ".operaciones::multiplicar($n1,$n2)."</div>";
                        break;
                        case 3:
                            echo "<div class=\"alert alert-success\" role=\"alert\">la división de los dos numeros ingresados es: ".operaciones::dividir($n1,$n2)."</div>";
                        break;
                        default:  //en caso de no seleccionar ningun operador matematico alertar 
                            echo '<div class="alert alert-danger" role="alert">
                             ! La operacion seleccionada no existe, por favor seleccione una operacion valida !
                          </div>';;
                        break;
                    }
                }

            }
        
    ?>
    </div>
<!-- Footer -->
<footer class="page-footer font-small dark">

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Luis Fernando Loaiza Acevedo: SENA
    <a href="#"> DESARROLLO WEB CON PHP</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->
    

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
    </script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
</body>

</html>